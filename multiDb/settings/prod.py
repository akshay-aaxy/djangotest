from multiDb.settings.base import *

DEBUG = False

ALLOWED_HOSTS = ["localhost"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'databaseone',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
    },
    'db2': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'databasetwo',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
    },
    'db3': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'databasethree',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '',
    },
    'db4': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'databasefour',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '',
    },
    'db5': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'databasefive',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '',
    },
}
