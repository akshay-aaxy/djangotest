############### Project Description ###############

The project is build with Django=1.10 & Python=3.6
Database used are Postgres & Mysql, psycopg2=2.7.5, mysqlclient=1.3.13

################ Project Setup #################

To setup the project create virtual environment with python=3.6
=> virtualenv -p python3.6 <name-of-env>

Activate the environment
=> source <name-of-env>/bin/activate

Clone project from repository
=> git clone https://akshay-aaxy@bitbucket.org/akshay-aaxy/djangotest.git

Navigate to the project & Install requirements.txt in virtual env
=> pip install -r requirements.txt

Update database setting
=> In base.py, local.py & prod.py for all 5 databases

Run the migration for all of the databases, there are five databases so run all below
=> python manage.py migrate --database=default
=> python manage.py migrate --database=db2
=> python manage.py migrate --database=db3
=> python manage.py migrate --database=db4
=> python manage.py migrate --database=db5

Update SMTP details in the settings.py file with your email_id & password
=> EMAIL_HOST_USER = 'Email id'
=> EMAIL_HOST_PASSWORD = 'Password'

Note:- For SMTP, google might block email sending, so you have to go to your gmail account and activate the Less Secure option. If needed please follow below link
https://support.google.com/cloudidentity/answer/6260879?hl=en 

############# Project Startup ###############

To run the project with default settings
=> python manage.py runserver

To run the project with local settings
=> python manage.py runserver --settings=multiDb.settings.local

To run the project with prod settings
=>python manage.py runserver --settings=multiDb.settings.prod