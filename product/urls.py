from django.conf.urls import url
from product.views import *

urlpatterns = [
    url(r'^add-product$', AddProductView.as_view(), name='add-product'),
    url(r'^list-product$', ListProduct.as_view(), name='list-product'),
]
