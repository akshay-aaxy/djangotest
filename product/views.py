from django.shortcuts import render
from django.contrib import messages
from django.db import transaction
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy

from users.models import UserProfile
from product.models import Product

@method_decorator(lambda x: login_required(x, login_url=reverse_lazy('login')),
    name='dispatch')
class AddProductView(View):
    template_name = 'addProduct.html'

    def get(self, *args, **kwargs):
        user = self.request.user
        # get all assigned db from M2M relationship if any
        assigned_db = user.userprofile.assigned_db.all()
        ctx = {
            'assigned_db': assigned_db,
        }
        return render(self.request, self.template_name, ctx)

    @transaction.atomic
    def post(self, *args, **kwargs):
        user = self.request.user
        name = self.request.POST.get('name', None)
        quantity = self.request.POST.get('quantity', None)
        selectedDb = self.request.POST.get('selectedDb', None)
        # create product in selected db instance
        Product.objects.using(selectedDb).create(name=name, 
            quantity=quantity, user_pk=user.pk)
        messages.success(self.request, 'Product Added Successfully')
        # get list of all assigned db to the user
        assigned_db = self.request.user.userprofile.assigned_db.all()
        ctx = {
            'assigned_db': assigned_db,
        }
        return render(self.request, self.template_name, ctx)

@method_decorator(lambda x: login_required(x, login_url=reverse_lazy('login')),
    name='dispatch')
class ListProduct(View):
    template_name = 'displayProduct.html'

    def get(self, *args, **kwargs):
        user = self.request.user
        # get all db assign to the user
        assigned_db = self.request.user.userprofile.assigned_db.all()
        result = []
        for assigned in assigned_db:
            if assigned.instance:
                result.append({
                    'databaseName': assigned.name,
                    'products': Product.objects.using(
                        assigned.instance).filter(user_pk=user.pk),
                })
        ctx = {
            'result': result,
        }
        return render(self.request, self.template_name, ctx)
