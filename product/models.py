from django.db import models
from users.models import UserProfile

class Product(models.Model):
	name = models.CharField(null=True, blank=True, max_length=50)
	quantity = models.CharField(null=True, blank=True, max_length=50)
	user_pk = models.CharField(null=True, blank=True, max_length=25)

	def __str__(self):
		return self.name
