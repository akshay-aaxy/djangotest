from django.db.models import Q
from django.conf import settings
from django.db import transaction
from django.contrib import messages 
from django.core.mail import send_mail
from django.utils.html import strip_tags
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy

from django.views.generic import View
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login, logout

from product.models import Product
from users.models import Databases, UserProfile

class LoginView(View):
    template_name = 'login.html'

    def get(self, *args, **kwargs):
        return render(self.request, self.template_name, {})

    def post(self, *args, **kwargs):
        username = self.request.POST.get('username', None)
        password = self.request.POST.get('password', None)
        if username and password:
            # check is there any user present with these credentials
            user = authenticate(username=username, password=password)
            if user is not None:
                # check is user active or not
                if user.is_active:
                    # if all correct add user to the session
                    login(self.request, user)
                    return redirect('home')
                else:
                    messages.error(self.request, 'User is inactive.')
            else:
                messages.error(self.request, 'Username or Password does not match.')
        return render(self.request, self.template_name, {})

class Logout(View):
    template_name = 'login.html'

    def get(self, *args, **kwargs):
        logout(self.request)
        return redirect('login')

def home(request):
    user_home_page = 'home.html'
    return render(request, user_home_page, {})

@method_decorator(lambda x: login_required(x, login_url=reverse_lazy('login')),
    name='dispatch')
class AddUserView(View):
    template_name = 'addUser.html'

    def get(self, *args, **kwargs):
        """
        To display add user page
        """
        databases = Databases.objects.all()
        ctx = {
            'databases': databases,
        }
        return render(self.request, self.template_name, ctx)

    @transaction.atomic
    def post(self, *args, **kwargs):
        """
        This method save's the user and user profile data.
        It also store all the assigned database to the user
        """
        fname = self.request.POST.get('fname', None)
        lname = self.request.POST.get('lname', None)
        username = self.request.POST.get('username', None)
        email = self.request.POST.get('email', None)
        password = self.request.POST.get('password', None)
        assigned_db = self.request.POST.getlist('selectedDb', None)
        databases = Databases.objects.all()
        try:
            # to check if the user or email is already exist or not
            user_obj = User.objects.get(Q(username=username) | Q(email=email))
            messages.error(self.request, 'Username or Email already exist.')
        except ObjectDoesNotExist:
            # first add user to the user model
            user_obj = User(first_name=fname, last_name=lname, username=username,
                email=email)
            # set password
            user_obj.set_password(password)
            user_obj.save()
            # create user profile with the user model
            user_profile_obj = UserProfile(user=user_obj)
            user_profile_obj.save()
            databases_obj = databases.filter(pk__in=assigned_db)
            # add all selected database instances to the user profile
            for database in databases_obj:
                user_profile_obj.assigned_db.add(database)
                user_profile_obj.save()
            messages.success(self.request, 
                'User added successfully, an email is sent with the '+ 
                'credentials to the user.')
            # after successfull creation of user it will send mail to user with credentials
            self.sendEmail(email, username, password)
            ctx = {
                'databases': databases,
            }
            return redirect('add-user')

    @staticmethod
    def sendEmail(email, username, password):
        """
        static method because we do not need 'self' here
        """
        ctx = {
            'username': username,
            'password': password,
        }
        html_ctx = render_to_string("email.html", ctx)
        html_message = strip_tags(html_ctx)
        subject = 'Welcome to Multi Db System'
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [email]
        send_mail(subject, html_message, email_from, recipient_list)
        return True

@method_decorator(lambda x: login_required(x, login_url=reverse_lazy('login')),
    name='dispatch')
class DisplayUserAndProductView(View):
    template_name = 'displayUserAndProduct.html'

    def get(self, *args, **kwargs):
        users_obj = UserProfile.objects.filter(user__is_superuser=False)
        result = []
        for user_obj in users_obj:
            for instance in user_obj.assigned_db.all():
                product_obj = Product.objects.using(
                    instance.instance).filter(user_pk=user_obj.user.pk)
                if product_obj:
                    result.append({
                        'user': user_obj.user.username,
                        'database': instance.name,
                        'product_obj': product_obj,
                    })
        ctx = {
            'result': result,
        }
        return render(self.request, self.template_name, ctx)
