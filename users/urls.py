from django.conf.urls import url
from users.views import *

urlpatterns = [
    url(r'^$', LoginView.as_view(), name='login'),
    url(r'^logout$', Logout.as_view(), name='logout'),
    url(r'^home$', home, name='home'),
    url(r'^add-user$', AddUserView.as_view(), name='add-user'),
    url(r'^display-user-and-product$', DisplayUserAndProductView.as_view(),
    	name='display-user-and-product'),
]
