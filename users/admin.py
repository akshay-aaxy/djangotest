from django.contrib import admin
from users.models import UserProfile, Databases

admin.site.register(UserProfile)
admin.site.register(Databases)