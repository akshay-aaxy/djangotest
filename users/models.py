from django.db import models
from django.contrib.auth.models import User

class Databases(models.Model):
	instance = models.CharField(null=True, blank=True, max_length=50)
	name = models.CharField(null=True, blank=True, max_length=50)

	def __str__(self):
		return self.name

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	assigned_db = models.ManyToManyField(Databases, blank=True,
		related_name="user_assigned_db")

	def __str__(self):
		return self.user.username
